#高雄榮美教會首頁

# Commit 規範

## Type本次commit提交的類型
- feat：新功能（feature）.
- fix：修補bug.
- docs：文檔（documentation）.
- style： 格式（不影響代碼運行的變動）
- refactor：重構（即不是新增功能，也不是修改bug的代碼變動）
- test：增加測試
- chore：構建過程或輔助工具的變動

### 每一次的commit都需要 title + type + message

[title]<br/>
type-message

#### Ex: 

#### [表單修正]<br/>
feat-新增表單輸入功能

#### [Bug]<br/>
fix-修正xxx bug

#### [新增圖片]<br/>
docs-新增xxx圖片

#### [修改縮排]<br/>
style-排版2個空格改成4個

#### [Redux]<br/>
refactor-react中加入redux

#### [手機測試]<br/>
test-測試手機板是否有問題

#### [套件更新]<br/>
chore-新增某某套件

### 因為在開發版本每個人都看的到你的訊息，所以嚴禁無異議的commit，請為每個與你共同開發的人著想
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
