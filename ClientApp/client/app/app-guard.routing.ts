import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AppService } from './shared/services/main.service';
import { MnFullpageService } from 'ngx-fullpage';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AppGuard implements CanActivate {
  isMoblie: boolean;
  constructor(
    private router: Router,
     public service: AppService,
    public fullpageService: MnFullpageService
     ) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.checkMobile();
    return true;
  }

  checkMobile() {
    return this.service.observeRWD().subscribe(result => {
        // 判斷尺寸是否為手機大小
      if (result.matches) {
        this.router.navigate(['/moblie']);
        // this.fullpageService.setAutoScrolling(false);
      } else {
        this.router.navigate(['/homepage']);
        // this.fullpageService.setAutoScrolling(true);
      }
    });
  }
}
