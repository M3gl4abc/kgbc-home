import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CrazyJesusComponent } from './crazy-jesus/components/crazy-jesus/crazy-jesus.component';
import { LeaderComponent } from './leader-tea-time/components/leader-tea-time/leader.component';
import { SinbaKingdomComponent } from './sinba-kingdom/components/sinba-kingdom/sinba-kingdom.component';
import { HistoryComponent } from './article/components/history/history.component';
import { MainPageComponent } from './mainpage/components/mainpage/mainpage.component';

const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'crazyjesus', component: CrazyJesusComponent },
  { path: 'leaderTeaTime', component: LeaderComponent },
  { path: 'sinbaKingdom', component: SinbaKingdomComponent },
  { path: 'article/history', component: HistoryComponent },
];

@NgModule({
  // imports: [RouterModule.forRoot(routes, { useHash: true })],
  imports: [
    RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload'}),
    BrowserAnimationsModule
  ],
  exports: [RouterModule]

})
export class AppRoutingModule { }
