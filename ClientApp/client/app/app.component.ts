import { Component, Input , OnInit} from '@angular/core';
import { AppService } from './shared/services/main.service';
import { RouterOutlet, Router } from '@angular/router';
import { MnFullpageService } from 'ngx-fullpage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})
export class AppComponent implements OnInit {

  constructor(
    public service: AppService
  ) {}
  ngOnInit(): void {

  }
}
