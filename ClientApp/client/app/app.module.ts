
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrazyJesusModule } from './crazy-jesus/crazy-jesus.module';
import { LeaderTeaTimeModule } from './leader-tea-time/leader-tea-time.module';
import { SinbaKingdomModule } from './sinba-kingdom/sinba-kingdom.module';
import { ArticleModule } from './article/article.module';
import { MainpageModule } from './mainpage/mainpage.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MainpageModule,
    CrazyJesusModule,
    LeaderTeaTimeModule,
    SinbaKingdomModule,
    ArticleModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
