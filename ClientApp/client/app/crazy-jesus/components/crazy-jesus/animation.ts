import { trigger, state, style, transition, animate } from '@angular/animations';
export const jesusBg = trigger('aniJesusBg', [
  transition('void => *', [
    style({
      filter: 'blur(5px)',
    }),
    animate('1s ease-in',
      style({
        filter: 'blur(0px)'
      }),
    )
  ]),
]);
export const jesusLogo = trigger('aniJesusLogo', [
  transition('void => *', [
    style({
      filter: 'blur(5px)',
      transform: 'translate(0, 100px)',
      opacity: 0,
    }),
    animate('1s ease-in',
      style({
        filter: 'blur(0px)',
        transform: 'translate(0, 0px)',
        opacity: 1,
      })
    )
  ]),
]);
export const jesusTxt = trigger('aniJesusTxt', [
  transition('void => *', [
    style({
      filter: 'blur(5px)',
      transform: 'translate(-150px, 0px)',
      opacity: 0,
    }),
    animate('1s ease-in',
      style({
        filter: 'blur(0px)',
        transform: 'translate(0, 0px)',
        opacity: 1,
      })
    )
  ])
]);
// export const jesusBg = trigger('aniJesusBg', [
//   state(':enter', style({
//     filter: 'blur(0px)',
//     height: '100%'
//     // transform: 'translate(0%, 0px)'
//   })),
//   state(':leave', style({
//     filter: 'blur(5px)',
//     height: '95%'
//     // transform: 'translate(-80%, 0px)'
//   })),
//   transition('* => crazyjesus', animate('1.5s  ease')),
// ]);
// export const jesusLogo = trigger('aniJesusLogo', [
//   state(':enter', style({
//     filter: 'blur(0px)',
//     transform: 'translate(0, 0px)',
//     opacity: 1,
//   })),
//   state(':leave', style({
//     filter: 'blur(5px)',
//     transform: 'translate(0, 100px)',
//     opacity: 0,
//   })),
//   transition('* => crazyjesusLogo', animate('1s ease-in')),
// ]);
// export const jesusTxt = trigger('aniJesusTxt', [
//   state(':enter', style({
//     filter: 'blur(0px)',
//     transform: 'translate(0, 0px)',
//     opacity: 1,
//   })),
//   state(':leave', style({
//     filter: 'blur(5px)',
//     transform: 'translate(-150px, 0px)',
//     opacity: 0,
//   })),
//   // animate(delay time ease)
//   transition('* => crazyjesusTxt', animate('1s ease-in')),
// ]);
