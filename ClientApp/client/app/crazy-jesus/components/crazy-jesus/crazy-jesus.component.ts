import { Component, OnInit, HostListener, Input, AfterContentInit } from '@angular/core';
import * as crazyJesus from './animation';
import { AppService } from '../../../shared/services/main.service';
import { MnFullpageService } from 'ngx-fullpage';
import { MatDialog } from '@angular/material/dialog';
import { OverviewComponent } from 'client/app/shared/components/overview/overview.component';

@Component({
  selector: 'crazy-jesus',
  templateUrl: './crazy-jesus.component.html',
  styleUrls: ['./crazy-jesus.component.scss'],
  animations: [
    crazyJesus.jesusBg,
    crazyJesus.jesusLogo,
    crazyJesus.jesusTxt
  ],
})
export class CrazyJesusComponent implements OnInit, AfterContentInit {

  @Input() detail: boolean;
  loading = false;
  loadingBG = false;
  mousewheel = false;
  constructor(
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {
  }
  ngAfterContentInit() {
    if (!this.loading) {
      this.loading = true;
      this.loadingBG = true;
    }
  }
  @HostListener('mousewheel', ['$event'])
  onScroll(event) {
    if (this.mousewheel === false) {
      this.mousewheel = true;
      this.loading = false;
      setTimeout(() => {
        this.dialog.open(
          OverviewComponent,
          {
            width: '1000px',
            height: '650px',
            data: {
              article: ''
            }
          }
        ).afterClosed().subscribe(() => {
          this.mousewheel = false;
          this.loading = true;
        });
      }, 0);
    }
  }
}
