import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';
import { MaterialModule } from '../shared/material/material.module';
import { MnFullpageModule } from 'ngx-fullpage';
import { CrazyJesusComponent } from './components/crazy-jesus/crazy-jesus.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    CrazyJesusComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
  ]
})
export class CrazyJesusModule { }
