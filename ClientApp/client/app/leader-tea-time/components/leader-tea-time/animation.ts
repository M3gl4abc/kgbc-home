import { trigger, state, style, transition, animate, group } from '@angular/animations';

export const textEn = trigger('aniLeaderEn', [
  transition('void => *', group([
    style({
      transform: 'translate(100px, -50px) rotate(-10deg)',
      color: 'transparent',
    }),
    animate('1s ease-in',
      style({
        transform: 'translate(0px, 0px) rotate(-10deg)',
      })
    ),
    animate('1.5s ease-in',
      style({
        color: '#fff',
      })
    ),
  ]))
]);

export const textZh = trigger('aniLeaderZh', [
  transition('void => *', [
    style({
      transform: 'translate(-5px, 10px) rotate(-8deg)',
      letterSpacing: '10px',
      opacity: 0,
    }),
    animate('1s ease-in',
      style({
        transform: 'translate(0px, 0px) rotate(-8deg)',
        letterSpacing: '0px',
        opacity: 1,
      })
    )
  ]),
]);
