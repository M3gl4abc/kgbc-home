import { Component, OnInit, AfterContentInit, HostListener } from '@angular/core';
import * as leaderTeaTime from './animation';
import { AppService } from '../../../shared/services/main.service';
import { MatDialog } from '@angular/material/dialog';
import { OverviewComponent } from 'client/app/shared/components/overview/overview.component';
import { article } from '../../../shared/article/leaderTeaTime';

@Component({
  selector: 'leader-cover',
  templateUrl: './leader.component.html',
  styleUrls: ['./leader.component.scss'],
  animations: [
    leaderTeaTime.textEn,
    leaderTeaTime.textZh
  ]
})
export class LeaderComponent implements OnInit, AfterContentInit {
  loading = false;
  article = article;
  mousewheel = false;
  constructor(
    public service: AppService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
  }
  ngAfterContentInit() {
    console.log(123);
    if (!this.loading) {
      this.loading = true;
      // console.log(this.loading);
    }
  }
  @HostListener('mousewheel', ['$event'])
  onScroll(event) {
    // console.log(event);
    // if (this.mousewheel === false) {
    //   this.mousewheel = true;
    //   this.loading = false;
    //   setTimeout(() => {
    //     this.dialog.open(
    //       OverviewComponent,
    //       {
    //         width: '1000px',
    //         height: '650px',
    //         data: {
    //           article: this.article
    //         }
    //       }
    //     ).afterClosed().subscribe(() => {
    //       this.mousewheel = false;
    //       this.loading = true;
    //     });
    //   }, 0);
    // }
  }
}
