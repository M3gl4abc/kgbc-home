import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaderComponent } from './components/leader-tea-time/leader.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '../app-routing.module';
import { MaterialModule } from '../shared/material/material.module';

@NgModule({
  declarations: [
    LeaderComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
  ]
})
export class LeaderTeaTimeModule { }
