import { trigger, transition, style, animate, state, query, stagger, group } from '@angular/animations';

export const aniMobile = trigger('aniMobile', [
  transition(':enter', [
    style({
      opacity: 0,
      transform: 'translateY(-100px)',
    }),
    group([
      animate('350ms cubic-bezier(.55,0,.1,1)',
        style({
          opacity: 1,
          transform: 'none',
        })
      ),
      query('.menu-moblie__item,.menu-moblie__locale-item', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)',
        }),
        stagger(-30, [
          animate('500ms cubic-bezier(0.35, 0, 0.25, 1)',
            style({
              opacity: 1,
              transform: 'none',
            }))
        ])
      ])
    ]),
  ]),
  transition(':leave', [
    group([
      animate('350ms cubic-bezier(.55,0,.1,1)',
        style({
          opacity: 0,
          transform: 'translateY(-100px)',
         })
      ),
      query('.menu-moblie__item,.menu-moblie__locale-item', [
        stagger(-30, [
          animate('500ms cubic-bezier(0.35, 0, 0.25, 1)',
            style({
              opacity: 0,
              transform: 'translateX(-100%)',
            }))
        ])
      ]),
    ])
  ]),
]);

