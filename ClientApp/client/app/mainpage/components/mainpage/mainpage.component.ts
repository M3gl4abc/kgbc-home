import { Component, OnInit, HostListener, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { aboutList } from 'client/app/shared/article/aboutList';
import { aniMobile,  } from './animation';
import { InstagramService } from 'client/app/shared/services/instagram.service';
import * as AOS from 'aos';
import { AgmInfoWindow } from '@agm/core';
import { I18nService } from 'client/app/shared/i18n/i18n.service';
import { NavbarService } from '../../service/navbar.service';
import { AboutService } from '../../service/about.service';
import { EveryWhereService } from '../../service/map/every-where.service';
import { ChruchMapService } from '../../service/map/chruch-map.service';
import { GoaService } from '../../../shared/services/goa.service';
@Component({
  selector: 'mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss'],
  animations: [
    aniMobile,
  ]
})
export class MainPageComponent implements OnInit, AfterViewInit {
  header = true;
  mediaList: any;
  aboutList = aboutList;
  isNavbarinTop: boolean;
  navbarList: any[];
  mobileMenuOpen = false;
  boardConfig = {
    slidesPerView: 3,
    spaceBetween: 30,
    freeMode: true,
    pagination: false
  };
  boardConfigMoblie = {
    slidesPerView: 1,
    spaceBetween: 30,
    freeMode: true,
    pagination: false
  };
  aboutChurch: any[];
  localeValue = '';
  localeFlag = '';
  @ViewChild('infowindow', { static: false }) infowindow: AgmInfoWindow;
  @ViewChild('bulletinBoard', { static: false }) bulletinBoard: ElementRef;
  @ViewChild('aboutChruch', { static: false }) aboutChruch: ElementRef;
  @ViewChild('everywhereMap', { static: false }) everywhereMap: ElementRef;
  @ViewChild('churchInfo', { static: false }) churchInfo: ElementRef;
  @ViewChild('footer', { static: false }) footer: ElementRef;


  constructor(
    public instagramService: InstagramService,
    public i18n: I18nService,
    public navbarService: NavbarService,
    public aboutService: AboutService,
    public everyWhereMap: EveryWhereService,
    public goaService: GoaService,
    public ChruchMap: ChruchMapService
  ) {
    this.aboutChurch = this.aboutService.getList();
    this.navbarList = this.navbarService.getList();
    this.mediaList = this.instagramService.getMedias();
  }
  ngOnInit(): void {
    AOS.init();
    this.localeValue = 'zh';
    this.localeFlag = this.i18n.getLangFlag(this.localeValue);

  }
  ngAfterViewInit(): void {
    const loading: HTMLElement  = document.querySelector('.loading');
    this.infowindow.open();
    loading.style.display = 'none';
    this.goaService.setTagDom(window,
      [
        this.bulletinBoard,
        this.aboutChruch,
        this.everywhereMap,
        this.churchInfo,
        this.footer
      ]
    )
  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    this.goaService.srcollTracking();
    if (window.pageYOffset > 0) {
      this.isNavbarinTop = true;
    } else {
      this.isNavbarinTop = false;
    }

  }
  showMoblieMenu(open) {
    this.mobileMenuOpen = open;
  }
  changeLang(key) {
    this.i18n.setLang(key);
    this.localeFlag = this.i18n.getLangFlag(key);
  }
  showMore(item) {
    const detail = this.aboutService.queryList(item);
    this.aboutService.openMore(detail);
  }
}
