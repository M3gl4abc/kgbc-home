import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './components/mainpage/mainpage.component';
import { MaterialModule } from '../shared/material/material.module';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SharedModule } from '../shared/shared.module';
import { NavbarService } from './service/navbar.service';
import { AboutService } from './service/about.service';
import { EveryWhereService } from './service/map/every-where.service';
import { ChruchMapService } from './service/map/chruch-map.service';


@NgModule({
  declarations: [
    MainPageComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SwiperModule,
    SharedModule
  ],
  exports: [
    MainPageComponent
  ],
  providers: [
    AboutService,
    NavbarService,
    EveryWhereService,
    ChruchMapService
  ]
})
export class MainpageModule { }
