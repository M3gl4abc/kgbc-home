import { Injectable } from '@angular/core';
import { ListBase } from './listbase';
import { MatDialog } from '@angular/material/dialog';
import { OverviewComponent } from 'client/app/shared/components/overview/overview.component';
import { aboutList as detailList } from 'client/app/shared/article/aboutList';

@Injectable()
export class AboutService extends ListBase {
  protected aboutMap = new Map();
  // private aboutList = [];
  public detailList = detailList;
  constructor(
    public dialog: MatDialog
  ) {
    super();
    this.initMap();
  }
  openMore(item) {
    if (!item.active) { return; }
    if (item.value === 'history') {
      window.open(
        `${window.location.origin}/article/history`,
        '_blank'
      );
    } else {
      const dialogRef = this.dialog.open(
        OverviewComponent,
        {
          width: '100%',
          height: '100%',
          maxWidth: '100%',
          data: item
        }
      );
      dialogRef.afterClosed().subscribe(() => {

      });
    }
  }
  initMap() {
    this.map.set(0, {
      title: '教會介紹',
      content: '高雄榮美教會的核心價值是堅持真理」、「奉獻心志」、「追求 合一」、「終身學習」',
      imgUrl: 'assets/img/photo/11.png',
      key: 'introduction'
    });
    this.map.set(1, {
      title: '金門榮美教會',
      content: '高雄榮美教會的核心價值是堅持真理」、「奉獻心志」、「追求 合一」、「終身學習」',
      imgUrl: 'assets/img/photo/10.jpg',
      key: 'kinmen'
    });
    this.map.set(2, {
      title: '開拓故事',
      content: '榮美教會由荷蘭宣教士萬牧師夫婦來台開拓建立而成,陳思恩牧師與陳憶湘師母於2009年正式接 下高雄榮美教會牧會工作',
      imgUrl: 'assets/img/photo/13.png',
      key: 'story'
    });
    this.map.set(3, {
      title: '展望未來',
      content: '亞洲新灣區是高雄市正全力建造的重點地區，將結合觀光、文創、商業、航運 等多面向發展',
      imgUrl: 'assets/img/photo/14.png',
      key: 'future'
    });
    this.map.set(4, {
      title: '榮美大記事',
      content: '高雄榮美教會歷史編年大事記',
      imgUrl: 'assets/img/photo/15.png',
      key: 'history'
    });
    this.map.set(5, {
      title: '城市參與',
      content: '2018.08.12　教會首次舉辦城市級的公民活動－「市長給問嗎?」，邀請當時三位高雄市長參選 人前來（當天出席兩位）',
      imgUrl: 'assets/img/photo/16.png',
      key: 'city'
    });
    this.map.set(6, {
      title: '宣教行動',
      content: '那年冬天他竟然來到金門，度過了一個跟上帝角 力的聖誕節。沒想到在12年後，思恩牧師在2017年的聖誕節再次回到金門！',
      imgUrl: 'assets/img/photo/17.png',
      key: 'mssionary'
    });
    this.map.set(7, {
      title: '事工部門',
      content: '從缺',
      imgUrl: 'assets/img/photo/12.png',
      key: 'ministry'
    });
    this.mapToArray();
  }
  queryList(q) {
    return detailList.find((e) => e.key === q);
  }
}
