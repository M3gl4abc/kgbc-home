export class ListBase {
  protected map = new Map();
  protected list = [];
  mapToArray() {
    this.map.forEach((val, key) => {
      this.list.push({
        key: key,
        ...val
      });
    });
  }
  getList() {
    return this.list;
  }
}
