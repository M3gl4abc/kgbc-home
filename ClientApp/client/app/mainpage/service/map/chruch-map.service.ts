import { Injectable } from '@angular/core';

@Injectable()
export class ChruchMapService {
  zoom = 17;
  latitude = 22.5978348;
  longitude = 120.3046204;
  kaohsiungLatitude = 22.5978348;
  kaohsiungLongitude = 120.3046204;
}
