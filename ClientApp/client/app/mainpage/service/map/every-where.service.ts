import { Injectable } from '@angular/core';
import { everywhereMapStyle } from 'client/app/shared/google-map/style';

@Injectable()
export class EveryWhereService {
  styles = everywhereMapStyle;
  scrollwheel = false;
  zoom = 7;
  latitude = 23.6755588;
  longitude = 120.6024256;
  kaohsiungLatitude = 22.5978348;
  kaohsiungLongitude = 120.3046204;
}
