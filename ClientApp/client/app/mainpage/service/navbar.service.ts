import { Injectable } from '@angular/core';
import { ListBase } from './listbase';

@Injectable()
export class NavbarService extends ListBase {
  constructor() {
    super();
    this.initNavbarMap();
  }
  initNavbarMap() {
    this.map.set(0, {
      text: '認識教會',
      link: '#aboutChruch',
      newPage: false,
      more: []
    });
    this.map.set(1, {
      text: 'Photos',
      link: 'https://www.instagram.com/jesus_design.kaohsiung/',
      newPage: true,
      more: []
    });
    this.map.set(2, {
      text: 'KGBC TV',
      link: 'https://www.youtube.com/channel/UCg5K9jpvqFhZTORYvlHEnuw',
      newPage: true,
      more: []
    });
    this.map.set(3, {
      text: '聚會資訊',
      link: '#infoChruch',
      newPage: false,
      more: []
    });
    this.map.set(4, {
      text: '我們的品牌',
      link: '',
      newPage: false,
      more: [
        {
          text: '李德踢炭',
          link: '/leaderTeaTime',
          newPage: true,
        },
        {
          text: '辛巴王國',
          link: '/sinbaKingdom',
          newPage: true,
        },
        {
          text: '瘋耶穌',
          link: '/crazyjesus',
          newPage: true,
        }
      ]
    });
    this.map.set(5, {
      text: '奉獻連結',
      link: 'https://service.kgbc.com.tw',
      newPage: true,
      more: []
    });
    this.mapToArray();
  }


}
