import { Component, OnInit, ViewChild, ElementRef, HostListener, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OverviewComponent } from 'client/app/shared/components/overview/overview.component';
import { Observable } from 'rxjs';
import { InstagramService } from 'client/app/shared/services/instagram.service';

import { KgbcTvService } from 'client/app/shared/services/kgbc-tv.service';
import { OverPlayComponent } from 'client/app/shared/components/over-play/over-play.component';
import { aboutList } from 'client/app/shared/article/aboutList';
import { Router } from '@angular/router';

@Component({
  selector: 'root-moblie',
  templateUrl: './root-moblie.component.html',
  styleUrls: ['./root-moblie.component.scss']
})
export class RootMoblieComponent implements OnInit, AfterViewInit {
  isMenuShow: boolean;
  aboutList = aboutList;
  hoverItem = 'introduction';
  scrollPosition: number;
  homePosition: number;
  aboutPosition: number;
  mediaPosition: number;
  kgbcTvPosition: number;
  index = 0;
  photoArray: Array<any>;
  listYT: Observable<any>;
  location = {
    title : 'Angular4 AGM Demo',
    lat : 22.5978348,
    lng : 120.3046204,
    zoomValue : 17,
    isOpen : true
  };
  state = 'hide';
  @ViewChild('infowindow', { static: false }) infowindow: ElementRef;
  @ViewChild('moblieHome', { static: false }) moblieHome: ElementRef;
  @ViewChild('aboutChruch', { static: false }) aboutChruch: ElementRef;
  @ViewChild('mediaPhoto', { static: false }) mediaPhoto: ElementRef;
  @ViewChild('kgbcTv', { static: false }) kgbcTv: ElementRef;
  constructor(
    public dialog: MatDialog,
    private mediaService: InstagramService,
    private tvService: KgbcTvService,
    private router: Router
    ) {
    this.isMenuShow = false;
  }

  ngOnInit() {
      this.mediaService.getMedias().subscribe((res: any) => {
        this.photoArray = res;
    });
    this.listYT = this.tvService.getYoutubeAPI();
  }
  ngAfterViewInit(): void {
    this.scrollPosition = window.pageYOffset;
    this.homePosition = this.moblieHome.nativeElement.offsetTop - 200;
    this.aboutPosition = this.aboutChruch.nativeElement.offsetTop - 200;
    this.mediaPosition = this.mediaPhoto.nativeElement.offsetTop - 200;
    this.kgbcTvPosition = this.kgbcTv.nativeElement.offsetTop - 200;

  }
  toggleMenu() {
    this.isMenuShow = !this.isMenuShow;
  }
  openArticle(item) {
    if (!item.active) { return; }
    if (item.value === 'history') {
      // console.log(item.value);
      this.router.navigate(['/article/history']);
    } else {
      const dialogRef = this.dialog.open(
        OverviewComponent,
        {
          width: '100%',
          height: '100%',
          maxWidth: '100%',
          data: item
        }
      );
      dialogRef.afterClosed().subscribe(() => {
      });
    }

  }
  onNextItem(i) {
    this.index = (this.index + i + this.photoArray.length) % this.photoArray.length;
    // console.log(this.index );
  }
  openYoutube(item) {
    const dialogRef = this.dialog.open(
      OverPlayComponent,
      {
        data: {
          type: 'youtube',
          url: item
        }
      }
    );
  }
  openVideo(item) {
    const dialogRef = this.dialog.open(
      OverPlayComponent,
      {
        data: {
          type: 'normal',
          url: item
        }
      }
    );
  }
  @HostListener('window:scroll', ['$event'])
  scrollListen() {
    this.scrollPosition = window.pageYOffset;

  }
}
