import { Component, OnInit } from '@angular/core';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import * as about from './animation';
import { AppService } from '../../../shared/services/main.service';
import { MnFullpageService } from 'ngx-fullpage';
import { MatDialog } from '@angular/material/dialog';
import { OverviewComponent } from 'client/app/shared/components/overview/overview.component';
import { aboutList } from 'client/app/shared/article/aboutList';
import { Router } from '@angular/router';

@Component({
  selector: 'about-church',
  templateUrl: './about-church.component.html',
  styleUrls: ['./about-church.component.scss'],
  animations: [
    about.bgimg
  ],
})
export class AboutChurchComponent implements OnInit {
  public config: PerfectScrollbarConfigInterface = {
    wheelSpeed : 1,
    swipeEasing: true
  };
  isOpen = true;
  introduction: boolean;
  story: boolean;
  future: boolean;
  ministry: boolean;
  aboutList = aboutList;
  readingArticle: string;
  hoverItem = 'introduction';
  isMobile: boolean;
  constructor(
    public service: AppService,
    private fullpageService: MnFullpageService,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.introduction = false;
    this.story = false;
    this.future = false;
    console.log(window.location);
  }
  setOverlay(name, active) {
    if (!active) { return; }
    this.readingArticle = name;
    this.fullpageService.setAllowScrolling(false);
  }
  closeOverlay(name) {
    this.readingArticle = name;
    this.fullpageService.setAllowScrolling(true);
  }
  openArticle(item) {
    if (!item.active) { return; }
    if (item.value === 'history') {
      window.open(
        `${window.location.origin}/article/history`,
        '_blank'
      );
    } else {
      this.fullpageService.setAllowScrolling(false);
      const dialogRef = this.dialog.open(
        OverviewComponent,
        {
          width: '100%',
          height: '100%',
          maxWidth: '100%',
          data: item
        }
      );
      dialogRef.afterClosed().subscribe(() => {
        this.fullpageService.setAllowScrolling(true);
      });
    }

  }
}
