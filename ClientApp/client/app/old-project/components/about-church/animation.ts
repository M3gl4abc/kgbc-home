import { trigger, state, style, transition, animate, group } from '@angular/animations';

export const bgimg = trigger('aniAboutBg', [
  state('about', style({
    transform: 'scale(1.1, 1.1)'
  })),
  state('*', style({
    transform: 'scale(1,1)'
  })),
  transition('* <=> about', animate('4s ease-out')),
]);
export const abBox = trigger('aniBox', [
  state('about', style({
    opacity: 1,
    transform: 'scaleX(1)'
  })),
  state('*', style({
    opacity: 0,
    transform: 'scaleX(0.5)'
  })),
  transition('* => about', animate('1s  cubic-bezier(0.55, 0.31, 0.15, 0.93)')),
]);
