import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'church-information',
  templateUrl: './church-information.component.html',
  styleUrls: ['./church-information.component.scss']
})
export class ChurchInformationComponent implements OnInit {
  title = 'Angular4 AGM Demo';
  lat = 22.5978348;
  lng = 120.3046204;
  zoomValue = 17;
  isOpen = true;
  @ViewChild('infowindow', { static: false }) infowindow: ElementRef;
  constructor() { }
  ngOnInit() {

  }
  onMouseOver(infoWindow) {

    // if (gm.lastOpen != null) {
    //     gm.lastOpen.close();
    // }
    //   gm.lastOpen = infoWindow;
      infoWindow.open();
  }
}
