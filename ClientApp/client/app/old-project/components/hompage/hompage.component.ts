import { Component, OnInit, Input } from '@angular/core';
import * as homePage from './animation';
import { MnFullpageOptions, MnFullpageService, } from 'ngx-fullpage';
import { AppService } from 'client/app/shared/services/main.service';

@Component({
  selector: 'hompage',
  templateUrl: './hompage.component.html',
  styleUrls: ['./hompage.component.scss'],
  animations: [
    homePage.bgimg
  ]
})
export class HompageComponent implements OnInit {

  constructor(
    public service: AppService,
    public fullpageService: MnFullpageService
  ) { }
  pageAnchor = '';
  @Input() public options: MnFullpageOptions = MnFullpageOptions.create({
    controlArrows: true,
    scrollingSpeed: 1000,
    navigation: true,
    css3: true,
    menu: '.menu',
    licenseKey: '83317442-245F40AC-A79A122F-FEE27EA6',
    normalScrollElements: '.scrollbar',
    anchors: [
      'homepage',
      'about',
      'media',
      'kgbcTV',
      'information',
      'footer'
    ],
    onLeave: (index, nextIndex, direction) => {
      this.changePage(nextIndex.anchor);
    }
  });
  shareToggle = false;
  ngOnInit(): void {
  }
  changePage(nextIndex) {
    this.pageAnchor = nextIndex;
    this.service.setPage(this.pageAnchor);
  }

}
