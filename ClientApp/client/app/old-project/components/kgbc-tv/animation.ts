import { trigger, state, style, transition, animate, group } from '@angular/animations';

export const bgimg = trigger('aniTvBg', [
  state('kgbcTV', style({
    transform: 'scale(1, 1)'
  })),
  state('*', style({
    transform: 'scale(0.9, 0.9)'
  })),
  transition('* <=> kgbcTV', animate('4s ease-out')),
]);
export const fadein = trigger('fadein', [
  transition('void => *', [
    style({
      opacity: 0,
      transform: 'translateX(50px)'
    }),
    animate('.5s ease-out', style({
      opacity: 1,
      transform: 'translateX(0px)'
    })),
  ]),
  // transition('* => void', [
  //   style({
  //     opacity: 1,
  //     transform: 'translateX(0px)'
  //   }),
  //   animate('.5s ease-in', style({
  //     opacity: 0,
  //     transform: 'translateX(-50px)'
  //   }))
  // ])
]);
