import { Component, OnInit } from '@angular/core';
import { KgbcTvService } from '../../../shared/services/kgbc-tv.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import * as kgbcTv from './animation';
import { AppService } from 'client/app/shared/services/main.service';
import { MatDialog } from '@angular/material/dialog';
import { OverPlayComponent } from 'client/app/shared/components/over-play/over-play.component';

@Component({
  selector: 'kgbc-tv',
  templateUrl: './kgbc-tv.component.html',
  styleUrls: ['./kgbc-tv.component.scss'],
  animations: [
    kgbcTv.fadein,
    kgbcTv.bgimg
  ]
})
export class KgbcTvComponent implements OnInit {

  listYT: Observable<any>;
  playVideo: String;
  lightboxShow = false;
  index = 0;
  maxIndex: number;
  constructor(
    private tvService: KgbcTvService,
    public service: AppService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.listYT = this.tvService.getYoutubeAPI();
    this.listYT.subscribe((result: Array<any> ) => {
      this.maxIndex = result.length;
    });
  }
  onNextPage(page) {
    this.index = (this.index + page + this.maxIndex) % this.maxIndex;
  }

  showYTVedio(videoId) {
    this.lightboxShow = true;
    this.playVideo = `https://www.youtube.com/embed/${videoId}?autoplay=1`;
  }
  cancelYTVedio() {
    this.lightboxShow = false;
    this.playVideo = null;
  }
  openYoutube(item) {
    const dialogRef = this.dialog.open(
      OverPlayComponent,
      {
        data: {
          type: 'youtube',
          url: item
        }
      }
    );
  }
  openVideo(item) {
    const dialogRef = this.dialog.open(
      OverPlayComponent,
      {
        data: {
          type: 'normal',
          url: item
        }
      }
    );
  }
}
