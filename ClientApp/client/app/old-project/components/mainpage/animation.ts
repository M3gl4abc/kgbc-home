import { trigger, state, style, transition, animate, group } from '@angular/animations';

export const bgimg = trigger('aniHomeBg', [
  state('homepage', style({
    transform: 'scale(1.05, 1.05)'
  })),
  state('*', style({
    transform: 'scale(1,1)'
  })),
  transition('* <=> homepage', animate('4s ease-out')),
]);
