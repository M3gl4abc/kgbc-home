import { Component, OnInit } from '@angular/core';
import * as homePage from './animation';
import { AppService } from 'client/app/shared/services/main.service';


@Component({
  selector: 'mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss'],
  animations: [
    homePage.bgimg
  ]
})
export class MainpageComponent implements OnInit {
  showMenu = false;
  isMobile: boolean;
  constructor(
    public service: AppService
  ) { }
  ngOnInit() {
    this.service.setPage('homepage');
    this.service.observeRWD().subscribe(result => {
      this.isMobile = result.matches;
    });
  }

}
