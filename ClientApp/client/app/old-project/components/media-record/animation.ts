import { trigger, state, style, transition, animate } from '@angular/animations';

export const photo = trigger('aniImag', [
  state('media', style({
    opacity: 1,
    transform: 'translateX(0px)'
  })),
  state('*', style({
    opacity: 0,
    transform: 'translateX(-100px)'
  })),
  transition('* <=> media', animate('1.5s cubic-bezier(0.55, 0.31, 0.15, 0.93)')),
]);
