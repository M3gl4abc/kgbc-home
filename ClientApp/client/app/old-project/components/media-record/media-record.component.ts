import { Component, OnInit } from '@angular/core';
import { AppService } from 'client/app/shared/services/main.service';
import { InstagramService } from '../../../shared/services/instagram.service';
import * as mediaRecord from './animation';
import { Observable } from 'rxjs';
@Component({
  selector: 'media-record',
  templateUrl: './media-record.component.html',
  styleUrls: ['./media-record.component.scss'],
  animations: [
    mediaRecord.photo
  ],
})
export class MediaRecordComponent implements OnInit {
  lightbox = true;
  photoArray: Observable<any>;
  selectPhoto = '';
  isMobile: boolean;
  constructor(
    public service: AppService,
    private mediaService: InstagramService
  ) { }

  ngOnInit() {
    this.service.observeRWD().subscribe(result => {
      this.isMobile = result.matches;
    });
    this.photoArray = this.mediaService.getMedias();
  }
  showPhoto(url) {
    this.selectPhoto = url;
    this.lightbox = false;
  }
}
