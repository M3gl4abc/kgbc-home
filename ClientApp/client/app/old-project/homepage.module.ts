import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HompageComponent } from './components/hompage/hompage.component';
import { MediaRecordComponent } from './components/media-record/media-record.component';
import { ChurchInformationComponent } from './components/church-information/church-information.component';
import { AboutChurchComponent } from './components/about-church/about-church.component';
import { VisionProjectComponent } from './components/vision-project/vision-project.component';
import { FooterComponent } from './components/footer/footer.component';
import { MainpageComponent } from './components/mainpage/mainpage.component';
import { KgbcTvComponent } from './components/kgbc-tv/kgbc-tv.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '../app-routing.module';
import { MnFullpageModule } from 'ngx-fullpage';
import { PerfectScrollbarModule, PerfectScrollbarConfigInterface, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../shared/material/material.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  declarations: [
    HompageComponent,
    MediaRecordComponent,
    ChurchInformationComponent,
    AboutChurchComponent,
    VisionProjectComponent,
    FooterComponent,
    MainpageComponent,
    KgbcTvComponent
  ],
  exports: [
    HompageComponent,
    MediaRecordComponent,
    ChurchInformationComponent,
    AboutChurchComponent,
    VisionProjectComponent,
    FooterComponent,
    MainpageComponent,
    KgbcTvComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MnFullpageModule.forRoot(),
    PerfectScrollbarModule,
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyBy1cQ5NLKyLJuQyo0paGrwpk8j1VKDMfM',
    //   language: 'zh-TW'
    // }),
    HttpClientModule,
    SharedModule,
    MaterialModule
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
})
export class HomepageModule { }
