export const article = `
<span>
  <br>
  <p class="title">
    李德踢碳
  </p>
  <br>
  <p class="title">
    ［關於我們］
  </p>
  <br>
  李德踢碳，是一個跨學校、跨社團、跨領域的互聯網教育平台，要為高雄校園的社團界，打造一個共創、共享、共榮的願景。
  <br>
  <br>
  <p class="title">
    ［我們的理念］
  </p>
  <br>
  <br>
  我們認為校園的文化跟社團有絕對性的關係，甚至一個人、一個社團就可以影響、改變一整個校園，因此我們期盼透過「李德踢碳」這個平台訓練出一群卓越的社團幹部，為他們所在的校園環境帶來正面的影響。  <br>
  我們相信，不管出身背景與家庭狀況如何，每個孩子都應享有充分的教育權。扶助孤兒寡母，更是神託付給基督徒的使命，也是我們要實現的社會正義。
</span>
`;
