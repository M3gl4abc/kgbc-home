import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'over-play',
  templateUrl: './over-play.component.html',
  styleUrls: ['./over-play.component.scss']
})
export class OverPlayComponent implements OnInit {
  playVideo: String;
  constructor(
    public dialogRef: MatDialogRef<OverPlayComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.type === 'youtube') {
       this.playVideo = `https://www.youtube.com/embed/${this.data.url}?autoplay=1`;
    }
    if (this.data.type === 'normal') {
      this.playVideo = this.data.url;
    }

  }

}
