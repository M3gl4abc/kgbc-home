import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class I18nService {
  protected langMap = new Map();
  private langList = [];
  constructor(
    private translateService: TranslateService
  ) {
    this.initLangMap();
  }
  initLangMap() {
    this.langMap.set('en', {
      flag: 'gb',
      language: 'English'
    });
    this.langMap.set('zh', {
      flag: 'tw',
      language: '繁體中文'
    });
    this.mapToArray();
  }
  mapToArray() {
    this.langMap.forEach((val, key) => {
      this.langList.push({
        key: key,
        ...val
      });
    });
  }
  getLangList() {
    return this.langList;
  }
  setLang(lang: string) {
    this.translateService.use(lang).subscribe((res)=>{
      // console.log(res)
    });
  }
  getLangFlag(key) {
    return this.langMap.get(key)['flag'];
  }
}
