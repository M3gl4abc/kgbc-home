import { of } from 'rxjs';

export class ServiceKit {
  formatErrors(error: any) {
    console.error(error);
    return of({ error });
  }
  chunkArray(arr: Array<any>, num: number) {
    const _num = num * 1 || 1;
    const _ret = [];
    arr.forEach((item, i) => {
      if (i % _num === 0) {
        _ret.push([]);
      }
      _ret[_ret.length - 1].push(item);
    });
    return _ret;
  }
}
