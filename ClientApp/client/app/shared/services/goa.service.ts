import { Injectable, HostListener, ElementRef } from '@angular/core';
declare let gtag: Function;
@Injectable({
  providedIn: 'root'
})
export class GoaService {
  domMap: Array<any>;
  win: Window;
  constructor() {

  }
  gTagTest() {
    gtag('event', 'click', {
      'event_category': 'nav_bar',
      'event_label': 'test',
      'value': 'dev test',
      'event_callback':  () => {
        console.log('test');
       }
    });
  }
  navBarTracking(text){
    gtag('event', 'click', {
      'event_category': 'nav_bar',
      'event_label': text,
      'event_callback': () => {
        console.log('ok');
      }
    });
  }
  srcollTracking() {
    this.domMap.forEach((i)=>{
      if (i.isSrcolled === true) {
        if (this.win.pageYOffset >= i.dom.nativeElement.offsetTop) {
          i.isSrcolled = false;
          gtag('event', 'Usability', {
            'event_category': 'srcoll_to',
            'event_label': i.dom.nativeElement.textContent,
            'event_callback': () => {
              console.log('ok');
            }
          });
        }
      }
    });
    this.isSrcollingEnd()
  }
  isSrcollingEnd(){
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    if (pos >= max){

      gtag('event', 'Usability', {
        'event_category': 'srcoll_end',
        'event_label': 'user finished',
        'event_callback': () => {
          console.log('end');
        }
      });
    }
  }

  setTagDom(win: Window,domList: Array<ElementRef>){
    this.win = win;
    this.domMap = domList.map((item) => {
      return {
        dom: item,
        isSrcolled: true
      };
    });
  }
}
