import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of, } from 'rxjs';
import { ServiceKit } from '../lib/serviceKit';
import { AppService } from './main.service';

@Injectable({
  providedIn: 'root'
})
export class InstagramService extends ServiceKit {
  private igApiUrl = 'https://www.instagram.com/jesus_design.kaohsiung/?__a=1';  // URL to web api
  constructor(
    private httpClinet: HttpClient,
    public service: AppService
  ) {
    super();
  }
  getMedias() {
    return this.httpClinet.get<any[]>(this.igApiUrl)
      .pipe(
        map((res: any) => super.chunkArray(res.graphql.user.edge_owner_to_timeline_media.edges, 10)),
        map((res) => res[0]),
        catchError(
          (err) => super.formatErrors(err)
        )
      );
  }

}
