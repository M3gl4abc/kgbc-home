import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of, } from 'rxjs';
import { ServiceKit } from '../lib/serviceKit';

@Injectable({
  providedIn: 'root'
})
export class KgbcTvService extends ServiceKit {
  private youtubeApiUrl = 'https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&key=AIzaSyBy1cQ5NLKyLJuQyo0paGrwpk8j1VKDMfM&channelId=UCg5K9jpvqFhZTORYvlHEnuw&order=date';
  constructor(private httpClinet: HttpClient) {
    super();
  }
  getYoutubeAPI() {
    return this.httpClinet.get<any[]>(this.youtubeApiUrl)
      .pipe(
        map((res: any) => super.chunkArray(res.items, 3) ),
        catchError(
          (err) => super.formatErrors(err)
        )
      );
  }
}
