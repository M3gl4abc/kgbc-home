import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  listerPage: string;
  constructor(
    private breakpointObserver: BreakpointObserver
  ) { }
  setPage(page) {
    this.listerPage = page;
  }
  getPage() {
    return this.listerPage;
  }
  observeRWD() {
    return this.breakpointObserver.observe([
      '(max-width: 1000px)',
    ]);
  }
}
