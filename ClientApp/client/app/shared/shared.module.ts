import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharemenuComponent } from './components/sharemenu/sharemenu.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { SafePipe } from './pipe/safeurl.pipe';
import { OverviewComponent } from './components/overview/overview.component';
import { OverPlayComponent } from './components/over-play/over-play.component';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TruncatePipe } from './pipe/truncate.pipe';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    SharemenuComponent,
    SafePipe,
    TruncatePipe,
    OverviewComponent,
    OverPlayComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBy1cQ5NLKyLJuQyo0paGrwpk8j1VKDMfM',
      language: 'zh-TW'
    }),
  ],
  exports: [
    SharemenuComponent,
    SafePipe,
    AgmCoreModule,
    FormsModule,
    TranslateModule,
    OverviewComponent,
    TruncatePipe,
    OverPlayComponent
  ],
  entryComponents: [
    OverviewComponent,
    OverPlayComponent
  ]
})
export class SharedModule { }
