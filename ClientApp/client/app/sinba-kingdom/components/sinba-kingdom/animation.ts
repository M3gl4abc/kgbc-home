import { trigger, transition, style, animate, group, query, stagger, animateChild } from '@angular/animations';

export const animal = trigger('animal', [
  transition(':enter', [
    query('li', [
      style({
        opacity: 0,
        transform: 'translate(0px, 200px)'
      }),
      stagger(100, [
        animate('.5s cubic-bezier(0.35, 0, 0.25, 1)',
        style({
           opacity: 1,
           transform: 'none'
        }))
      ])
    ])
  ]),
  transition(':leave', [
    query('li', [
      style({
        opacity: 1,
        transform: 'none'
      }),
      stagger(100, [
        animate('.5s cubic-bezier(0.35, 0, 0.25, 1)',
          style({
            opacity: 0,
            transform: 'translate(0px, 200px)'
          })
        )
      ])
    ])
  ])
]);
export const simbaLogo = trigger('simbaLogo', [
  transition('void => *', [
    style({
      transform: 'rotate(-30deg)',
      opacity: 0,
    }),
    animate('1s cubic-bezier(0.35, 0, 0.25, 1)',
      style({
        transform: 'rotate(0deg)',
        opacity: 1,
      })
    )
  ]),
]);
export const simbaNarrator = trigger('simbaNarrator', [
  transition('void => *', [
    style({
      transform: 'translate(0px, 200px)',
      opacity: 0,
    }),
    animate('1s cubic-bezier(0.35, 0, 0.25, 1)',
      style({
        transform: 'none',
        opacity: 1,
      })
    )
  ]),
]);
