import { Component, OnInit, AfterContentInit, HostListener } from '@angular/core';
import * as sinbaKingdom from './animation';
import { MatDialog } from '@angular/material/dialog';
import { OverviewComponent } from 'client/app/shared/components/overview/overview.component';
import { article } from '../../../shared/article/simba';
@Component({
  selector: 'sinba-kingdom',
  templateUrl: './sinba-kingdom.component.html',
  styleUrls: ['./sinba-kingdom.component.scss'],
  animations: [
    sinbaKingdom.animal,
    sinbaKingdom.simbaLogo,
    sinbaKingdom.simbaNarrator
  ]

})
export class SinbaKingdomComponent implements OnInit, AfterContentInit {
  constructor(
    public dialog: MatDialog,
  ) { }
  article = article;
  loading = false;
  mousewheel = false;
  animalArray = [
    {
      name: 'fox',
      url: 'assets/img/sinba/fox.png'
    },
    {
      name: 'sheep',
      url: 'assets/img/sinba/sheep.png'
    },
    {
      name: 'giraffe',
      url: 'assets/img/sinba/giraffe.png'
    },
    {
      name: 'zebra',
      url: 'assets/img/sinba/zebra.png'
    },
    {
      name: 'simba',
      url: 'assets/img/sinba/simba.png'
    },
    {
      name: 'panda',
      url: 'assets/img/sinba/panda.png'
    },
    {
      name: 'dog',
      url: 'assets/img/sinba/dog.png'
    },
    {
      name: 'pig',
      url: 'assets/img/sinba/pig.png'
    },
  ];


  ngOnInit() {
  }
  ngAfterContentInit() {
    this.loading = true;
  }
  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    if (window.pageYOffset > 200) {
      this.loading = false;
    } else {
      this.loading = true;
    }
  }
}

