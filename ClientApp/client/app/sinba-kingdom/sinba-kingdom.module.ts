import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SinbaKingdomComponent } from './components/sinba-kingdom/sinba-kingdom.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    SinbaKingdomComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
  ]
})
export class SinbaKingdomModule { }
